'use strict';

import 'mocha';
import { assert } from 'chai';
import { getApod } from '../src/commons/services';

describe('Service tests', () => {
    it.skip('getApod', async () => {
        const fact = await getApod();
        assert.isNotNull(fact);
        assert.isNotNull(fact.url);
    });
});