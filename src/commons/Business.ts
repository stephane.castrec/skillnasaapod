import { Constants } from './Constants';
import { getApod } from './services.js';
import { HandlerInput } from 'ask-sdk';
import { apl } from '../skill/template';
import { SkillInputUtils } from '../skill/SkillInputUtils';


export const playFact = async (input: HandlerInput) => {
    let msg;
    msg = Constants.messages.welcome();
    const apod = await getApod();
    if (new SkillInputUtils(input).hasApl()) {
        console.log("apod", apod);
        input.responseBuilder.speak(msg + ' ' + apod.title)
        input.responseBuilder.addDirective(apl(apod));
    } else {
        input.responseBuilder.speak(msg + ' ' + apod.title + '. ' + Constants.messages.no_screen())
            .withShouldEndSession(true);
    }
}   
