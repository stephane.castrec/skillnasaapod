import { getOne } from './utils/utils';


export const Constants = {
    messages: {
        welcome: () => {
            return 'bienvenue sur La tête dans les étoiles.'; 
        },
        no_screen: () => {
            return 'Vous n\'avez pas d\'écran sur se périphérique. Vous ne pourrez pas profiter de cette super image'; 
        },
        next: () => {
            return 'Et voilà une autre image.';
        },
        error: () => {
            return 'Il semblerait que chat mignon a un problème. Ré-essais dans un instant.';
        },
        help: (): string => {
            return "Grace aux données de la NASA,chaque jour une nouvelle image des étoiles.";
        },
        not_supported: () => {
            return "Cette skill ne supporte pas cet appareil";
        },
        bye: () => {
            return "A demain.";
        }
    }
}