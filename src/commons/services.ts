import * as request from 'request-promise-native';

const url = 'https://api.nasa.gov/planetary/apod?api_key=';
const key = "Q37QbJed8lJh8BT8TE8DM7FFZb4IR5wH9YyL8bFL";

/*
[{"id":"271","fact":"http:\/\/www.chucknorrisfacts.fr\/img\/upload\/q48da669cd83d2.jpg","date":"1222288034","vote":"5494","points":"21503"}]
*/
export interface Apod {
    id: string;
    url: string;
    title: string;
    media_type: string;
}

export const getApod = async (): Promise<Apod> => {
    var options = {
        uri:url + process.env.NASA_API_KEY,
        json: true
    };

    return request.get(options);
}


