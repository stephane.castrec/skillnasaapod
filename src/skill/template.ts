import { Directive } from "ask-sdk-model";
import  aplDoc from "./apls/apl";
import { Apod } from "../commons/services";

const getAplParam = (imgUrl, title) => {
    return {
        "bodyTemplate7Data": {
            "type": "object",
            "objectId": "bt7Sample",
            "title": title,
            "image": {
                "contentDescription": null,
                "smallSourceUrl": null,
                "largeSourceUrl": null,
                "sources": [
                    {
                        "url": imgUrl,
                        "size": "small",
                        "widthPixels": 0,
                        "heightPixels": 0
                    },
                    {
                        "url": imgUrl,
                        "size": "large",
                        "widthPixels": 0,
                        "heightPixels": 0
                    }
                ]
            },
            "logoUrl": "https://chuckfacts.s3-eu-west-1.amazonaws.com/chucknorris_large.png",
        }
    }
}

export const apl = (apod: Apod): Directive => {
    return {
        type: 'Alexa.Presentation.APL.RenderDocument',
        document: aplDoc,
        datasources: getAplParam(apod.url, apod.title)
    };
}